<?php

use DIC\DIC;

echo '<pre>';

require 'DIC.php';
require 'Database/Connection.php';
class Foo
{

    private string $uniqId;
    private string $car;

    public function __construct( string $car = "citadine")
    {
        $this->uniqId = uniqid();
        $this->car = $car;
    }

}


class Bar
{

    /**
     * @var Foo
     */
    private  $foo;
    private string $uniqId;
    private string $deuxRoues;


    public function __construct(Foo $foo, $deuxRoues = 'velo')
    {

        $this->foo = $foo;
        $this->uniqId = uniqid();
        $this->velo = $deuxRoues;
    }

}

$app = new DIC();

//Exemple pour le factory "nouvelle instance a chaque appel de la methode get($key)"
/*
$app->setFactory('Bar', function () use($app){
    return new Bar($app->get('Foo'));
});
var_dump($app->get('Bar'));
*/



//Exemple si je veux initialiser les champs moi-même j'utilise la methode set
$app->set('Bar', function (){
    return new Bar(new Foo(), "Moto");
});
var_dump($app->get('Bar'));

?>

<hr>
<?php
//Exemple
var_dump($app->get('Foo'));
var_dump($app->get('Foo'));
var_dump($app->get('Foo'));



echo '</pre>';