<?php

namespace DIC;

use Exception;
use ReflectionClass;
use ReflectionException;

class DIC
{
    /**
     * @var array contient les différentes dépendances
     */
    private array $regestry = [];
    /**
     * @var array contient les différentes dependances pour avoir toujours une nouvelle instance
     */
    private array $factories = [];

    /**
     * @var array permet de sauvegarder les différents instances des différentes dépendances
     */
    private array $instances = [];

    /**
     * @var array permet de sauvegarder l'ensemble des paramètres du constructeur
     */
    private array $contructor_parameters = [];


    /**
     * @param $key (est les clé de mon instance)
     * @param callable $resolver (function qui permet de retourner l'instance)
     * je créer une nouvelle instance et la sauvegarde dans mon tableau registry
     */
    public function set($key, Callable $resolver)
    {
        $this->regestry[$key] = $resolver;
    }


    /**
     * @param $key (est les clé de mon instance)
     * @param callable $resolver (function qui permet de retourner l'instance)
     * je créer l'instance qui aura à chaque appel de la methode get une nouvelle instance
     */
    public function setFactory($key, callable $resolver)
    {
        $this->factories[$key] = $resolver;
    }

    /**
     * @param $instance
     * @throws ReflectionException
     * j'instancie ReflexionClass permet d'analiser une class ou un objet et de recupérer ses informations
     * je passe une clé à mon tableau instances qui correspond au nom de l'instance
     * et je sauvegarde dans le tableau instances le nom de la classe qui sera passée en paramètre de la fonction setInstance
     */
    public function setInstance($instance)
    {
        $reflection = new ReflectionClass($instance);
        $this->instances[$reflection->getName()] = $instance;
    }

    /**
     * Fonction qui permet de retourner une instance avec ou sans les paramètres du constructeur
     * La même instance sera créée a chaque appel de la function get($key) sauf si l'instance de la classe
     * à été sauvegardée dans le tableau factories
     * @param $key (La classe que l'on veut obtenir)
     * @return mixed
     * @throws ReflectionException
     * @throws Exception
     */
    public function get($key): mixed
    {
        /**
         * Si j'ai des instances dans mon tableau factories, je return l'instance
         * Une nouvelle instance sera créée a chaque appel de la function get
         * @see setFactory()
         */
        if (isset($this->factories[$key]))
        {
            return $this->factories[$key]();
        }

        /**
         * si je n'ai pas d'instance dans mon tableau instances
         */
        if(!isset($this->instances[$key]))
        {
            /**
             * si j'ai des instances dans le tableau registry voir fonction set($key, Callable $resolver)
             * alors je les sauvegarde dans le tableau instances
             */
            if (isset($this->regestry[$key]))
            {

                $this->instances[$key] = $this->regestry[$key]();

            }else {

                $reflected_class = new ReflectionClass($key);

                /**
                 * si $reflected_class contient la class et qu'elle est instanciable cela return true
                 */
                if($reflected_class->isInstantiable())
                {
                    /**
                     * je recupère le constructeur de la classe
                     */
                    $constructor = $reflected_class->getConstructor();

                    /**
                     * je verifie si j'ai un constructeur sinon je créer une nouvelle instance
                     * sans passer de paramètre à la methode newInstance()
                     */
                    if ($constructor)
                    {

                        /**
                         * je recupère les paramètres du constrcutor et je boucle dessus
                         */
                        foreach ($constructor->getParameters() as $parameter)
                        {
                            $type = $parameter->getType();

                            /**
                             * Return un object de ReflectionType si un type de paramètre est spécifié, sinon null
                             * sinon si j'ai des paramètres facultatifs je les sauvegarde dans mon tableau constructor_parameters
                             */
                            if ($type)
                            {
                                /**
                                 * je verifie si la classe existe
                                 * si la classe existe alors je la sauvegarde dans mon tableau constructor_parameters
                                 */
                                if(class_exists($type->getName()))
                                {
                                    $name_class = $type->getName();
                                    $this->contructor_parameters[] = $this->get($name_class);
                                }

                            }else{
                                var_dump($parameter->isOptional());
                                if ($parameter->isOptional())
                                {
                                    $this->contructor_parameters[] = $parameter->getDefaultValue();
                                }
                            }

                        }

                        /**
                         * je construis l'instance en lui passant les paramètres
                         */
                        $this->instances[$key] = $reflected_class->newInstanceArgs($this->contructor_parameters);

                    }else{

                        $this->instances[$key] = $reflected_class->newInstance();
                    }

                }else{
                    throw new Exception($key ."isn't instanciable Class");
                }

            }
        }
        return $this->instances[$key];
    }



}
